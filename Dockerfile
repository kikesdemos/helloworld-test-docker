FROM openjdk:17-oracle
Volume /tmp
ADD /target/*.jar helloworld-test-docker-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/helloworld-test-docker-0.0.1-SNAPSHOT.jar"]