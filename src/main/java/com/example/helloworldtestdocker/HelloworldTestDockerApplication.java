package com.example.helloworldtestdocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloworldTestDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloworldTestDockerApplication.class, args);
	}

}
